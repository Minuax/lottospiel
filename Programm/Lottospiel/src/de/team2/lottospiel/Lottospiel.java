package de.team2.lottospiel;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Team 2
 * @version 1.0.4
 * Created: 28.01.20-31.01.20
 */

public class Lottospiel {

    /**
     * Hauptmethode des Programms.
     * Programmablauf:
     * 1. Willkommensnachricht
     * 2. Aufforderung, 6 Zahlen einzugeben (tippeLottozahlen())
     * 3. Sortierung der eingetippten Zahlen
     * 4. Zufälliges Ziehen von 6 Gewinnzahlen (zieheLottozahlen())
     * 5. Sortierung der Gewinnzahlen
     * 6. Ausgabe der eingetippten, sortierten Zahlen nach Muster:   Zahl1   Zahl2   Zahl3   Zahl4   Zahl5   Zahl6
     * 7. Ausgabe der gezogenen, sortierten Zahlen nach Muster:      Zahl1   Zahl2   Zahl3   Zahl4   Zahl5   Zahl6
     * 8. Vergleich der beiden Zahlenreihen, pruefen auf richtige
     * 9. Anzahl der richtigen ausgeben
     * 10. Zahlenstatistik drucken (druckeStatistik())
     * 11. Programm beenden
     *
     * @param args Startargumente
     */
    public static void main(String[] args) {
        int richtige = 0;
        System.out.println("Willkommen beim Lottospiel!");  // Splashscreen

        System.out.println("Bitte tippen Sie 6 Zahlen.");   // Aufforderung des Nutzers

        int[] getippt = tippeLottozahlen(); // Aufrufen der Methode 'tippeLottozahlen', welche die Nutzereingabe in dem Integer Array speichert
        int[] getipptSortiert = sortiere(getippt, 0, getippt.length - 1);   // getippte Zahlen werden mit Startparametern sortiert

        int[] gezogen = zieheLottozahlen(); // Computer generiert zufällige Lottozahlen
        int[] gezogenSortiert = sortiere(gezogen, 0, gezogen.length - 1);   // Generierte Zahlen werden sortiert

        System.out.print("Ihre Lottozahlen sind: ");    // Ausgabe der getippten Lottozahlen in richtiger Reihenfolge
        for (int i : getipptSortiert) { // Loop durch alle Zahlen
            System.out.print(i + "\t"); // Ausgabe, durch Tabulator getrennt
        }

        System.out.println();   // Sprung zur nächsten Zeile

        System.out.print("Gezogene Lottozahlen:  ");    // Ausgabe der generierten Lottozahlen (sortiert)
        for (int i : gezogenSortiert) { // Loop durch alle Zahlen
            System.out.print(i + "\t"); // Ausgabe, durch Tabulator getrennt
        }

        richtige = ermittleRichtige(getipptSortiert, gezogenSortiert);  // Anzahl der richtigen Zahlen wird per 'ermittleRichtige' ermittelt
        System.out.println("\nSie hatten " + richtige + " richtige.");  // Ausgabe der richtigen
    }

    /**
     * Methode, um den User dazu aufzufordern 6 Lottozahlen einzutippen.
     * Prueft ob Zahl zwischen 1 und 49 liegt und nicht schon einmal vorhanden ist.
     *
     * @return Rückgabe der getippten Zahlen
     */
    public static int[] tippeLottozahlen() {
        int[] zahlen = new int[6];  // Initialisiere integer array um lottozahlen zu speichern
        Scanner scanner = new Scanner(System.in);   // Initialisieren des Scanners

        for (int i = 0; i < 6; ++i) { // Wiederhole, bis i 6 ist
            System.out.print("Bitte Zahl " + (i + 1) + " eingeben: ");    // Aufforderung des Users
            String in = scanner.next();

            try{
                int eingabe = Integer.parseInt(in);
                if (pruefeZahlen(zahlen, eingabe)) {  // Pruefen der Zahlen
                    zahlen[i] = eingabe;    // Speicherung
                    System.out.println("Zahl " + eingabe + " wurde hinzugefügt.");  // Informieren des Users
                } else {
                    System.out.println("Diese Zahl ist bereits vorhanden oder ist zu groß/klein. Zahlenraum: 1-49.");   // Fehler: Eingabe fehlerhaft
                    i--;    // Zaehlvariabel wird zurückgesetzt
                }
            }catch (Exception ex){
                System.out.println("Bitte geben Sie eine Zahl ein.");
                i--;
            }
        }
        return zahlen;  // Eingelesene Zahlen werden zurückgegeben
    }

    /**
     * Zieht zufällig 6 Lottozahlen, welche anschließend auf Korrektheit geprueft werden.
     * Zufall wird geschaffen durch (int) (Math.random() * 49) + 1
     *
     * @return Rückgabe der zufälligen Zahlen
     */
    public static int[] zieheLottozahlen() {
        int[] zahlen = new int[6];  // Initialisiert int arry

        for (int i = 0; i < 6; ++i) { // Zählschleife, es müssen 6 Zahlen gezogen werden
            int zahl = (int) (Math.random() * 49) + 1;  // Zufallsgenerator

            if (pruefeZahlen(zahlen, zahl)) { // Pruefe Zahl
                zahlen[i] = zahl; // Zahl korrekt? Speichern
            } else {
                i--;    // Zahl inkorrekt? Wiederholen
            }
        }
        return zahlen;  // Rueckgabe der Zahlen
    }

    /**
     * Ermittelt wie viele richtige Zahlen der User getippt hat und gibt es als Ganzzahl zurück.
     *
     * @param getippt Eingabe des User-Tipps
     * @param gezogen Eingabe von zufällig generierten Zahlen
     * @return Rückgabe der richtigen
     */
    public static int ermittleRichtige(int[] getippt, int[] gezogen) {
        int count = 0;

        for (int i : getippt) {   // Loopt durch die getippten Zahlen
            for (int e : gezogen) {   // Loppt durch die gezogenen Zahlen
                if (i == e) { // Stimmen Zahlen überein?
                    count++;    // Anzahl richtige erhoehen
                }
            }
        }

        return count;   // Rueckgabe der Anzahl
    }

    /**
     * Rekursive Methode, welche Zahlen so lange von außen nach innen vergleicht, bis alle in der korrekten Reihenfolge sind.
     *
     * @param toSort Das Array, welches sortiert werden soll
     * @param links  Linke Anfang der Sortierung
     * @param rechts Rechter Anfang der Sortierung
     * @return Gibt das sortierte Array zurück
     */
    public static int[] sortiere(int[] toSort, int links, int rechts) {
        int splitPoint; // Punkt an welchem Gruppen geteilt werden
        if (links < rechts) {   // Kein Überlappen?
            splitPoint = zerteileReihe(toSort, links, rechts);  // Punkt wird per 'zerteileReihe' festgelegt
            sortiere(toSort, links, splitPoint);    // Rekursion mit anderen Parametern (links)
            sortiere(toSort, splitPoint + 1, rechts);   // Rekursion mit anderen Parametern (rechts)
        }
        return toSort;  //  Gebe sortiertes Array zurück
    }

    /**
     * Zerteilt und vergleicht die jeweilige Stelle mit der mittigen Zahl der Reihe (beidseitig)
     *
     * @param inputArray Das Array, welches verarbeitet werden soll
     * @param links      Der linke Startwert
     * @param rechts     Der rechte Startwert
     * @return Gibt das verarbeitete Array zurück
     */
    public static int zerteileReihe(int[] inputArray, int links, int rechts) {
        int linksKorrekt = links - 1, rechtsKorrekt = rechts + 1, splitPoint = inputArray[(links + rechts) / 2];

        while (true) {  // Endlosschleife, welche per return verlassen wird.
            do {
                linksKorrekt++; // mindestens eine Verschiebung
            } while (inputArray[linksKorrekt] < splitPoint);    // anschließende Prüfung, ob erneut verschoben werden muss

            do {
                rechtsKorrekt--;    // mindestens eine Verschiebung
            } while (inputArray[rechtsKorrekt] > splitPoint);   // anschließende Prüfung, ob erneut verschoben werden muss

            if (linksKorrekt < rechtsKorrekt) { // Gruppen überlappen? Mittlere Zahlen miteinander vergleichen
                int backupLinks = inputArray[linksKorrekt];   // Linke stelle wird 'gebackupt'
                inputArray[linksKorrekt] = inputArray[rechtsKorrekt];   // links wird auf rechts gesetzt
                inputArray[rechtsKorrekt] = backupLinks;    // Rechts wird auf Links gesetzt
            } else {
                return rechtsKorrekt;   // Gruppen überlappen nicht: Schleife wird verlassen
            }
        }
    }

    /**
     * Prueft Zahlen auf Korrektheit
     *
     * @param toread Vorhandene Zahlen
     * @param value  Zupruefende Zahl
     * @return Rückgabe ob korrekt oder nicht
     */
    public static boolean pruefeZahlen(int[] toread, int value) {
        if (value < 1 || value > 49) // Groesse der Zahl prüfen
            return false;   // Zahl zu klein/gross
        for (int i : toread) { // Zahlen durchgehen
            if (i == value)
                return false;   // Zahl bereits vorhanden
        }
        return true; // Kein fehler = alles okay
    }

}
