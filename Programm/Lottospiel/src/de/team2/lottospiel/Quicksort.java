package de.team2.lottospiel;

import java.util.Random;

/**
 * Testklasse für Quicksort-Funktion.
 * Beinhaltet Testmethoden für Operations- und Geschwindigkeitsmessung.
 * Hauptmethode: main(String[]) void
 * Quelle des ursprünglichen Codes und der Erklärung: https://de.wikipedia.org/wiki/Quicksort
 */
public class Quicksort {

    public static int operations;

    /**
     * Rekursive Methode, welche Zahlen so lange von außen nach innen vergleicht, bis alle in der korrekten Reihenfolge sind.
     * @param toSort    Das Array, welches sortiert werden soll
     * @param links     Linke Anfang der Sortierung
     * @param rechts    Rechter Anfang der Sortierung
     * @return          Gibt das sortierte Array zurück
     */
    public static int[] sortiere(int[] toSort, int links, int rechts) {
        operations++;
        int splitPoint;
        if (links < rechts) {
            splitPoint = zerteileReihe(toSort, links, rechts);
            sortiere(toSort, links, splitPoint);
            sortiere(toSort, splitPoint + 1, rechts);
        }
        return toSort;
    }

    /**
     * Zerteilt und vergleicht die jeweilige Stelle mit der mittigen Zahl der Reihe (beidseitig)
     * @param intArr    Das Array, welches verarbeitet werden soll
     * @param links     Der linke Startwert
     * @param rechts    Der rechte Startwert
     * @return          Gibt das verarbeitete Array zurück
     */
    public static int zerteileReihe(int[] intArr, int links, int rechts) {
        int linksKorrekt = links - 1, rechtsKorrekt = rechts + 1, splitPoint = intArr[(links + rechts) / 2];

        while (true) {
            do {
                linksKorrekt++;
                operations++;
            } while (intArr[linksKorrekt] < splitPoint);

            do {
                rechtsKorrekt--;
                operations++;
            } while (intArr[rechtsKorrekt] > splitPoint);

            if (linksKorrekt < rechtsKorrekt) {
                int k = intArr[linksKorrekt];
                intArr[linksKorrekt] = intArr[rechtsKorrekt];
                intArr[rechtsKorrekt] = k;
                operations++;
            } else {
                return rechtsKorrekt;
            }
        }
    }

    /**
     * Führt Quicksort mit 100000 zufällig generierten Zahlen zwischen 0 und 100000 durch
     * und misst dabei die Anzahl der nötigen Operationen und die dafür aufgewendete Zeit.
     * Zeitnahme: System.currentTimeMillis()
     * Operatoren: werden bei jeder durchgeführten Aktion um eins erhöht.
     * @param args
     */
    public static void main(String[] args) {
        long start, calculate, stop;
        int[] toSort = new int[100000000];
        Random random = new Random();

        operations = 0;

        for(int i = 0; i < 100000000; i++) {
            toSort[i] = Math.abs(random.nextInt(100000000));
            //before[i] = i;
        }

        start = System.currentTimeMillis();
        int[] arr = sortiere(toSort,0, toSort.length - 1);
        calculate = System.currentTimeMillis();

        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + 1 + ": " + arr[i]);
        }
        stop = System.currentTimeMillis();

        System.out.println("Gestartet bei: " + start + ", beendet bei: " + stop + ". Unterschied: " + (stop - start) + "(Reine Berechnung: " + (calculate - start));
        System.out.println("Operationen: " + operations);

    }

}
